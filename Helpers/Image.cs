﻿namespace VestjyskMarketing.Helpers
{
    public class ImageHelper
    {
        /// <summary>
        /// resize image. If no image, return placeholder
        /// </summary>
        /// <param name="url"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="crop"></param>
        /// <param name="compression"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static string ResizeImage(string url, int width = 200, int height = 200, int crop = 0, int compression = 85)
        {

            if (string.IsNullOrWhiteSpace(url))
            {
                url = string.Format("http://placehold.it/{0}x{1}", width, height);
            }
            if (!url.StartsWith("http"))
            {
                url = string.Format("/Admin/Public/GetImage.ashx?Image={0}&width={1}&height={2}&crop={3}&Compression={4}", url, width, height, crop, compression);
            }
            return url;
        }
    }
}