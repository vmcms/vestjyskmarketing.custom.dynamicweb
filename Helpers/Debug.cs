﻿using System;

namespace VestjyskMarketing.Helpers
{
    public class DebugHelper
    {
        /// <summary>
        ///   // https://github.com/dynamicweb/razor/wiki/Using-Json.Net-to-inspect-values
        ///   //dump object debug data to view
        /// </summary>
        /// <param name="value"></param>
        /// <param name="heading"></param>
        /// <returns></returns>
        public static string DumpObject(object value, string heading = "")
        {
            string body = Newtonsoft.Json.JsonConvert.SerializeObject(value, new Newtonsoft.Json.JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore,
                Error = (serializer, err) => err.ErrorContext.Handled = true,
                Formatting = Newtonsoft.Json.Formatting.Indented
            });

           return String.Format("<pre class='dw-dump'>{0}{1}</pre>", "<b>" + heading + "</b>", body);
        }
   }
}