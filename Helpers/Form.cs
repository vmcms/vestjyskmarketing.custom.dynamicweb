﻿using System;
using System.Collections.Generic;
using System.Web;

namespace VestjyskMarketing.Helpers
{

    public class FormHelper
    {
        /// <summary>
        /// Returns errors on validation
        /// </summary>
        /// <param name="formId"></param>
        /// <param name="validationClass"></param>
        /// <returns></returns>
        public static string ValidationErrors(int formId, string validationClass = "message error")
        {
            //if error, then return div'
            string html = "";

            //check if we have correct form and if we have an error
            bool showError = false;
            if (!String.IsNullOrEmpty((string)HttpContext.Current.Session["validation_error"]))
            {
                showError = true;
            }
            if (showError && formId != (int)HttpContext.Current.Session["validation_error_form_id"])
            {
                showError = false;
            }

            if (showError)
            {
                HttpContext.Current.Session["validation_error"] = null;
                HttpContext.Current.Session["validation_error_form_id"] = null;
                List<string> validationErrors = (List<string>)HttpContext.Current.Session["validation_error_form_errors"];

                foreach (string error in validationErrors)
                {
                    html = html + "<li>" + Dynamicweb.SystemTools.Translate.Translate("Husk at udfylde") + " " + error.ToString() + "</li>";
                }

                return String.Format("<div class='{0}' id='validation-error-{2}'><ul>{1}</ul></div>", validationClass, html, formId);
            }

            return "";
        }
    }
}