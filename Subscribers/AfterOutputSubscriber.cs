﻿using Dynamicweb.Extensibility.Notifications;
using Dynamicweb.Notifications;
using System;
using System.Web;

namespace VestjyskMarketing.Subscribers
{
    [Subscribe(Standard.Page.AfterOutput)]
    public class AfterOutputSubscriber : NotificationSubscriber
    {
        /// <summary>
        /// After output on page
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="args"></param>
        public override void OnNotify(string notification, NotificationArgs args)
        {
            Standard.Page.AfterOutputArgs afterOutputArgs = args as Standard.Page.AfterOutputArgs;

            //remove sessions if active on form error
            if (!String.IsNullOrEmpty((string)HttpContext.Current.Session["validation_error"]))
            {
                HttpContext.Current.Session["validation_error"] = null;
                HttpContext.Current.Session["validation_error_form_id"] = null;
                HttpContext.Current.Session["validation_error_form_errors"] = null;
            }
        }
    }
}