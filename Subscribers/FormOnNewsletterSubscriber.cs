﻿using Dynamicweb.Extensibility.Notifications;
using Dynamicweb.Forms;
using System;
using System.Web;
using System.Collections.Generic;

namespace VestjyskMarketing.Subscribers
{

    [Subscribe(Notifications.Frontend.OnAfterSubmitSave)]
    public class FormOnNewsletterSubscribe1 : NotificationSubscriber
    {
        /// <summary>
        /// After save on form for editors
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="args"></param>
        public override void OnNotify(string notification, NotificationArgs args)
        {
            string disabled = Dynamicweb.Configuration.SystemConfiguration.Instance.GetValue("/Globalsettings/VJM/FormServerValidationDisabled");
            
            if (disabled != "True")
            {
                //get args
                Notifications.Frontend.OnAfterSubmitSaveArgs newsArgs = args as Notifications.Frontend.OnAfterSubmitSaveArgs;

                //list of error
                List<string> validationErrors = new List<string>();

                //check for error
                bool validationFailed = false;
                foreach (Field field in newsArgs.Form.Fields)
                {
                    if (field.Required && field.Active)
                    {
                        if (field.Type != "File" && String.IsNullOrEmpty(HttpContext.Current.Request.Form[field.SystemName]))
                        {
                            validationFailed = true;
                        }

                        if (validationFailed)
                        {
                            validationErrors.Add(field.Name);
                        }
                    }
                }

                //if error
                if (validationFailed)
                {
                    //set session state
                    HttpContext.Current.Session["validation_error"] = "1";
                    HttpContext.Current.Session["validation_error_form_id"] = newsArgs.Form.ID;
                    HttpContext.Current.Session["validation_error_form_errors"] = validationErrors;

                    //delete entry
                    newsArgs.Submit.Delete();

                    //redirect back
                    var referer = HttpContext.Current.Request.UrlReferrer;
                    string AbsoluteUri = referer.AbsoluteUri;
                    string urlReturn = AbsoluteUri + "#validation-error-" + newsArgs.Form.ID;
                    HttpContext.Current.Response.Redirect(urlReturn, true);
                }


            }
        }
    }
}